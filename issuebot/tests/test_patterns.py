#!/usr/bin/env python3

import issuebot
import unittest

CODEBERG_DESCRIPTION = """
* [X] The app complies with the [inclusion criteria](https://f-droid.org/docs/Inclusion_Policy/?title=Inclusion_Policy).
* [X] The app is not already [listed](https://gitlab.com/search?scope=issues&group_id=28397) in the repo or issue tracker.
* [X] The app has not already [been requested](https://gitlab.com/search?scope=issues&project_id=2167965)
* [X] The upstream app source code repo contains the app metadata _(summary/description/images/changelog/etc)_ in a [Fastlane](https://gitlab.com/snippets/1895688) or [Triple-T](https://gitlab.com/snippets/1901490) folder structure
* [X] The original app author has been notified, and does not oppose the inclusion.
* [X] [Donated](https://f-droid.org/donate/) to support the maintenance of this app in F-Droid.

---------------------

#### APPLICATION ID: com.flasskamp.subz

```yaml
AutoName: Subz
Categories:
  - Money
License: MIT
AuthorName: Christian Flaßkamp
SourceCode: https://codeberg.org/epinez/Subz/
IssueTracker: https://codeberg.org/epinez/Subz/issues

RepoType: git
Repo: https://codeberg.org/epinez/Subz.git
```

Summary and description are in fastlane.

This is an Ionic based webapp. Would it be sufficient to create a new branch, e.g. `fdroid`, push the webassets into that branch and make tags with the name e.g. `v1.0-fdroid` of which the F-Droid bot would build the releases? Thanks in advance!
"""


class TestPatterns(unittest.TestCase):
    def test_git_pattern(self):
        urls = [
            'https://bitbucket.org/gbriggs/lbp_tcpipsockets',
            'https://bitbucket.org/tjk104/openfndds',
            'https://codeberg.org/epinez/Subz',
            'https://framagit.org/beriain/quicklyquit',
            'https://framagit.org/dystopia-project/simple-email',
            # 'https://git.code.sf.net/p/wifiremoteplay/android_native',  # TODO fix me
            'https://github.com/0xFireball/PluckLockEx',
            'https://github.com/GEANT/CAT-Android',
            'https://github.com/ucam-cl-dtg/barcodebox',
            'https://gitlab.com/fdroid/fdroidclient',
            'https://gitlab.com/x653/all_in_gold',
        ]
        for url in urls:
            self.assertEqual(url, issuebot.GIT_PATTERN.search(url).group())
        found = set()
        for m in issuebot.GIT_PATTERN.finditer(CODEBERG_DESCRIPTION):
            found.add(m.group())
        self.assertEqual(
            {
                'https://codeberg.org/epinez/Subz',
                'https://codeberg.org/epinez/Subz.git',
                'https://gitlab.com/snippets/1901490',
                'https://gitlab.com/snippets/1895688',
            },
            found,
        )

    def test_parse_git_urls_from_description(self):
        self.assertEqual(
            ['https://codeberg.org/epinez/Subz'],
            issuebot.parse_git_urls_from_description(CODEBERG_DESCRIPTION),
        )

    def test_parse_apk_download_urls(self):
        t = '### Link to app in another app store: %s\n\n### License used: Apache-2.0\n'
        apk_urls = [
            'https://apt.izzysoft.de/fdroid/repo/com.wilco375.onetwoauthenticate_25.apk'
        ]
        for url in apk_urls:
            self.assertEqual([url], issuebot.APK_DOWNLOAD_URL_PATTERN.findall(t % url))

        no_apk_urls = [
            'https://www.apkmirror.com/apk/sony-mobile-communications/pico-tts-2/pico-tts-2-1-0-release/pico-tts-1-0-4-android-apk-download/'
        ]
        for url in no_apk_urls:
            self.assertNotEqual(
                [url], issuebot.APK_DOWNLOAD_URL_PATTERN.findall(t % url)
            )

    def test_fastlane_locale_tag_pattern(self):
        valid_locale_tags = [
            'en-US',
            'en',
            'mas',
            'fr-CA',
            'es-419',
            'zh-Hans',
            'zh-Hant-HK',
        ]
        invalid_locale_tags = ['en_US', 'en-rUS']
        for locale_tag in valid_locale_tags:
            self.assertIsNotNone(
                issuebot.BCP47_LOCALE_TAG_PATTERN.fullmatch(locale_tag)
            )
        for locale_tag in invalid_locale_tags:
            self.assertIsNone(issuebot.BCP47_LOCALE_TAG_PATTERN.fullmatch(locale_tag))


if __name__ == "__main__":
    unittest.main()
